Pod::Spec.new do |s|
  s.name             = 'LGKKeyboardFit'
  s.version          = '1.0.1'
  s.summary          = '输入框键盘自适应'
  s.homepage         = 'https://gitee.com/yjhome/lgkkeyboardfit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'lyj' => 'liuyajun1999@icloud.com' }
  s.source           = { :git => 'https://gitee.com/yjhome/lgkkeyboardfit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '9.0'
  s.source_files = 'LGKKeyboardFit/Classes/**/*'
  s.resources = 'LGKKeyboardFit/Assets/LGKKeyboardFit.bundle'
  s.public_header_files = 'LGKKeyboardFit/Classes/**/*.h'
end
