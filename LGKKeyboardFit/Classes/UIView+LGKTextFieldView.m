//
//  UIView+LGKTextFieldView.m
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/25.
//

#import "UIView+LGKTextFieldView.h"
#import <objc/runtime.h>

/**
 Uses default keyboard distance for textField.
 */
CGFloat const kLGKUseDefaultKeyboardDistance = CGFLOAT_MAX;

@implementation UIView (LGKTextFieldView)
-(void)setKeyboardDistanceFromTextField:(CGFloat)keyboardDistanceFromTextField{
    //Can't be less than zero. Minimum is zero.
    keyboardDistanceFromTextField = MAX(keyboardDistanceFromTextField, 0);
    
    objc_setAssociatedObject(self, @selector(keyboardDistanceFromTextField), @(keyboardDistanceFromTextField), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(CGFloat)keyboardDistanceFromTextField{
    NSNumber *keyboardDistanceFromTextField = objc_getAssociatedObject(self, @selector(keyboardDistanceFromTextField));
    
    return (keyboardDistanceFromTextField != nil)?[keyboardDistanceFromTextField floatValue]:kLGKUseDefaultKeyboardDistance;
}

-(void)setIgnoreSwitchingByNextPrevious:(BOOL)ignoreSwitchingByNextPrevious{
    objc_setAssociatedObject(self, @selector(ignoreSwitchingByNextPrevious), @(ignoreSwitchingByNextPrevious), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)ignoreSwitchingByNextPrevious{
    NSNumber *ignoreSwitchingByNextPrevious = objc_getAssociatedObject(self, @selector(ignoreSwitchingByNextPrevious));
    
    return [ignoreSwitchingByNextPrevious boolValue];
}

- (void)setKeyboardBarEnable:(BOOL)keyboardBarEnable{
    objc_setAssociatedObject(self, @selector(keyboardBarEnable), @(keyboardBarEnable), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (BOOL)keyboardBarEnable{
    NSNumber *keyboardBarEnable = objc_getAssociatedObject(self, @selector(keyboardBarEnable));
    return [keyboardBarEnable boolValue];
}
-(void)setShouldResignOnTouchOutsideMode:(LGKEnableMode)shouldResignOnTouchOutsideMode{
    objc_setAssociatedObject(self, @selector(shouldResignOnTouchOutsideMode), @(shouldResignOnTouchOutsideMode), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(LGKEnableMode)shouldResignOnTouchOutsideMode{
    NSNumber *shouldResignOnTouchOutsideMode = objc_getAssociatedObject(self, @selector(shouldResignOnTouchOutsideMode));
    
    return [shouldResignOnTouchOutsideMode unsignedIntegerValue];
}

@end
