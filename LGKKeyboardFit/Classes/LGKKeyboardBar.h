//
//  LGKKeyboardBar.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/24.
//

#import "LGKTitleBarButtonItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface LGKKeyboardBar : UIToolbar<UIInputViewAudioFeedback>
@property(nonnull, nonatomic, strong) LGKBarButtonItem *previousBarButton;
@property(nonnull, nonatomic, strong) LGKBarButtonItem *nextBarButton;
@property(nonnull, nonatomic, strong, readonly) LGKTitleBarButtonItem *titleBarButton;
@property(nonnull, nonatomic, strong) LGKBarButtonItem *doneBarButton;
@property(nonnull, nonatomic, strong) LGKBarButtonItem *fixedSpaceBarButton;
@end

NS_ASSUME_NONNULL_END
