//
//  LGKFTextView.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LGKFTextView : UITextView
@property(nullable, nonatomic,copy) IBInspectable NSString *placeholder;
@property(nullable, nonatomic,copy) IBInspectable NSAttributedString *attributedPlaceholder;
@property(nullable, nonatomic,copy) IBInspectable UIColor *placeholderTextColor;
@end

NS_ASSUME_NONNULL_END
