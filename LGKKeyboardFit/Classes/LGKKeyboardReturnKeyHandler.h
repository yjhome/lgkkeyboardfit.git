//
//  LGKKeyboardReturnKeyHandler.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/24.
//

#import "LGKKeyboardFitConst.h"

NS_ASSUME_NONNULL_BEGIN
@class UITextField, UIView, UIViewController;
@interface LGKKeyboardReturnKeyHandler : NSObject
-(nonnull instancetype)initWithViewController:(nullable UIViewController*)controller NS_DESIGNATED_INITIALIZER;
-(nonnull instancetype)initWithCoder:(nullable NSCoder *)aDecoder NS_UNAVAILABLE;
@property(nullable, nonatomic, weak) id<UITextFieldDelegate,UITextViewDelegate> delegate;
@property(nonatomic, assign) UIReturnKeyType lastTextFieldReturnKeyType;
-(void)addTextFieldView:(nonnull UIView*)textFieldView;
-(void)removeTextFieldView:(nonnull UIView*)textFieldView;
-(void)addResponderFromView:(nonnull UIView*)view;
-(void)removeResponderFromView:(nonnull UIView*)view;
@end

NS_ASSUME_NONNULL_END
