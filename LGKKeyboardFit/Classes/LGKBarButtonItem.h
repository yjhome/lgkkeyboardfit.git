//
//  LGKBarButtonItem.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/24.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class NSInvocation;
@interface LGKBarButtonItem : UIBarButtonItem
@property (nonatomic, readonly) BOOL isSystemItem;
@property (nullable, strong, nonatomic) NSInvocation *invocation;
-(void)setTarget:(nullable id)target action:(nullable SEL)action;
@end

NS_ASSUME_NONNULL_END
