//
//  UIView+LGKTextFieldView.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/25.
//

#import <UIKit/UIKit.h>
#import "LGKKeyboardFitConst.h"
NS_ASSUME_NONNULL_BEGIN

/**
 Uses default keyboard distance for textField.
 */
extern CGFloat const kLGKUseDefaultKeyboardDistance;


@interface UIView (LGKTextFieldView)
/**
 To set customized distance from keyboard for textField/textView. Can't be less than zero
 */
@property(nonatomic, assign) CGFloat keyboardDistanceFromTextField;

/**
 If shouldIgnoreSwitchingByNextPrevious is YES then library will ignore this textField/textView while moving to other textField/textView using keyboard toolbar next previous buttons. Default is NO
 */
@property(nonatomic, assign) BOOL ignoreSwitchingByNextPrevious;
/**
 Override resigns Keyboard on touching outside of UITextField/View behaviour for this particular textField.
 */
@property(nonatomic, assign) LGKEnableMode shouldResignOnTouchOutsideMode;

/** 自适应键盘功能是否开启，默认为NO */
@property(nonatomic, assign) BOOL keyboardBarEnable;
@end

NS_ASSUME_NONNULL_END
