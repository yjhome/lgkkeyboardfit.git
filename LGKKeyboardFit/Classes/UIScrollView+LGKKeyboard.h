//
//  UIScrollView+LGKKeyboard.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (LGKKeyboard)
/**
 If YES, then scrollview will ignore scrolling (simply not scroll it) for adjusting textfield position. Default is NO.
 */
@property(nonatomic, assign) BOOL shouldIgnoreScrollingAdjustment;

/**
 Restore scrollViewContentOffset when resigning from scrollView. Default is NO.
 */
@property(nonatomic, assign) BOOL shouldRestoreScrollViewContentOffset;
@end


@interface UITableView (LGKKeyboard)
-(nullable NSIndexPath*)previousIndexPathOfIndexPath:(nonnull NSIndexPath*)indexPath;
@end


@interface UICollectionView (LGKKeyboard)
-(nullable NSIndexPath*)previousIndexPathOfIndexPath:(nonnull NSIndexPath*)indexPath;
@end
NS_ASSUME_NONNULL_END
