//
//  LGKKeyboardFitConst.h
//  Pods
//
//  Created by 刘亚军 on 2022/5/24.
//

#ifndef LGKKeyboardFitConst_h
#define LGKKeyboardFitConst_h
#import <UIKit/UIKit.h>
#import <Foundation/NSObjCRuntime.h>

typedef NS_ENUM(NSInteger, LGKAutoToolbarManageBehaviour) {
    LGKAutoToolbarBySubviews,
    LGKAutoToolbarByTag,
    LGKAutoToolbarByPosition,
};
typedef NS_ENUM(NSUInteger, LGKPreviousNextDisplayMode) {
    LGKPreviousNextDisplayModeDefault,
    LGKPreviousNextDisplayModeAlwaysHide,
    LGKPreviousNextDisplayModeAlwaysShow,
};
typedef NS_ENUM(NSUInteger, LGKEnableMode) {
    LGKEnableModeDefault,
    LGKEnableModeEnabled,
    LGKEnableModeDisabled,
};
#endif /* LGKKeyboardFitConst_h */
