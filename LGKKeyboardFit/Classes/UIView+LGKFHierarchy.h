//
//  UIView+LGKFHierarchy.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/25.
//

#import <UIKit/UIKit.h>
#import "LGKKeyboardFitConst.h"
NS_ASSUME_NONNULL_BEGIN

@class UICollectionView, UIScrollView, UITableView, UISearchBar, NSArray;
@interface UIView (LGKFHierarchy)
/** Returns the UIViewController object that manages the receiver. */
@property (nullable, nonatomic, readonly, strong) UIViewController *viewContainingController;

/** 最顶层UIViewController. */
@property (nullable, nonatomic, readonly, strong) UIViewController *topMostController;

/**
 Returns the UIViewController object that is actually the parent of this object. Most of the time it's the viewController object which actually contains it, but result may be different if it's viewController is added as childViewController of another viewController.
 */
@property (nullable, nonatomic, readonly, strong) UIViewController *parentContainerViewController;

/**
 Returns the superView of provided class type.

 @param classType class type of the object which is to be search in above hierarchy and return

 @param belowView view object in upper hierarchy where method should stop searching and return nil
 */
-(nullable __kindof UIView*)superviewOfClassType:(nonnull Class)classType belowView:(nullable UIView*)belowView;
-(nullable __kindof UIView*)superviewOfClassType:(nonnull Class)classType;

/**
 Returns all siblings of the receiver which canBecomeFirstResponder.
 */
@property (nonnull, nonatomic, readonly, copy) NSArray<__kindof UIView*> *responderSiblings;

/**
 Returns all deep subViews of the receiver which canBecomeFirstResponder.
 */
@property (nonnull, nonatomic, readonly, copy) NSArray<__kindof UIView*> *deepResponderViews;

/**
 Returns current view transform with respect to the 'toView'.
 */
-(CGAffineTransform)convertTransformToView:(nullable UIView*)toView;

/**
 Returns a string that represent the information about it's subview's hierarchy. You can use this method to debug the subview's positions.
 */
@property (nonnull, nonatomic, readonly, copy) NSString *subHierarchy;

/**
 Returns an string that represent the information about it's upper hierarchy. You can use this method to debug the superview's positions.
 */
@property (nonnull, nonatomic, readonly, copy) NSString *superHierarchy;

/**
 Returns an string that represent the information about it's frame positions. You can use this method to debug self positions.
 */
@property (nonnull, nonatomic, readonly, copy) NSString *debugHierarchy;
@end

@interface UIViewController (LGKFHierarchy)

-(nullable UIViewController*)parentLGKContainerViewController;

@end


@interface NSObject (LGKLogging)

/**
 Short description for logging purpose.
 */
@property (nonnull, nonatomic, readonly, copy) NSString *_LGKDescription;

@end
NS_ASSUME_NONNULL_END
