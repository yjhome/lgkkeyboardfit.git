//
//  LGKBarButtonItem.m
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/24.
//

#import "LGKBarButtonItem.h"

@implementation LGKBarButtonItem
// + initialize 在类第一次接收到消息之前被系统自动调用，无需手动调用。
+ (void)initialize{
    [super initialize];

    LGKBarButtonItem *appearanceProxy = [self appearance];

    NSArray <NSNumber*> *states = @[@(UIControlStateNormal),@(UIControlStateHighlighted),@(UIControlStateDisabled),@(UIControlStateSelected),@(UIControlStateApplication),@(UIControlStateReserved)];
    
    for (NSNumber *state in states)
    {
        UIControlState controlState = [state unsignedIntegerValue];

        [appearanceProxy setBackgroundImage:nil forState:controlState barMetrics:UIBarMetricsDefault];
        [appearanceProxy setBackgroundImage:nil forState:controlState style:UIBarButtonItemStyleDone barMetrics:UIBarMetricsDefault];
        [appearanceProxy setBackgroundImage:nil forState:controlState style:UIBarButtonItemStylePlain barMetrics:UIBarMetricsDefault];
        [appearanceProxy setBackButtonBackgroundImage:nil forState:controlState barMetrics:UIBarMetricsDefault];
    }

    [appearanceProxy setTitlePositionAdjustment:UIOffsetZero forBarMetrics:UIBarMetricsDefault];
    [appearanceProxy setBackgroundVerticalPositionAdjustment:0 forBarMetrics:UIBarMetricsDefault];
    [appearanceProxy setBackButtonBackgroundVerticalPositionAdjustment:0 forBarMetrics:UIBarMetricsDefault];
}
-(void)setTintColor:(UIColor *)tintColor{
    [super setTintColor:tintColor];

    NSMutableDictionary *textAttributes = [[self titleTextAttributesForState:UIControlStateNormal] mutableCopy]?:[NSMutableDictionary new];
    
    textAttributes[NSForegroundColorAttributeName] = tintColor;
    
    [self setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
}

- (instancetype)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem target:(nullable id)target action:(nullable SEL)action{
    self = [super initWithBarButtonSystemItem:systemItem target:target action:action];
    
    if (self){
        _isSystemItem = YES;
    }
    
    return self;
}


-(void)setTarget:(nullable id)target action:(nullable SEL)action{
    NSInvocation *invocation = nil;
    
    if (target && action){
        invocation = [NSInvocation invocationWithMethodSignature:[target methodSignatureForSelector:action]];
        invocation.target = target;
        invocation.selector = action;
    }
    
    self.invocation = invocation;
}

-(void)dealloc{
    self.target = nil;
    self.invocation = nil;
}
@end
