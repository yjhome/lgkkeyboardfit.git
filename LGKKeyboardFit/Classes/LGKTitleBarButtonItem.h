//
//  LGKTitleBarButtonItem.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/24.
//

#import <UIKit/UIKit.h>
#import "LGKKeyboardFitConst.h"
#import "LGKBarButtonItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface LGKTitleBarButtonItem : LGKBarButtonItem
@property(nullable, nonatomic, strong,readonly) UIView *titleView;
@property(nullable, nonatomic, strong,readonly) UIButton *titleButton;
@property(nullable, nonatomic, strong) UIFont *titleFont;
@property(nullable, nonatomic, strong) UIColor *titleColor;
@property(nullable, nonatomic, strong) UIColor *selectableTitleColor;
/**
NS_DESIGNATED_INITIALIZER:标明指定初始化方法
 */

- (nonnull instancetype)initWithTitle:(nullable NSString *)title NS_DESIGNATED_INITIALIZER;

/** NS_UNAVAILABLE：用来标明禁用父类的某些指定初始化方法即可，调用者调用NS_UNAVAILABLE标记的方法时，在编译阶段会报错提示 */
/**
 Unavailable. Please use initWithFrame:title: method
 */
-(nonnull instancetype)init NS_UNAVAILABLE;

/**
 Unavailable. Please use initWithFrame:title: method
 */
-(nonnull instancetype)initWithCoder:(nullable NSCoder *)aDecoder NS_UNAVAILABLE;

/**
 Unavailable. Please use initWithFrame:title: method
 */
+ (nonnull instancetype)new NS_UNAVAILABLE;
@end

NS_ASSUME_NONNULL_END
