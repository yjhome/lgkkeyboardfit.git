//
//  NSArray+LGKKeyboard.h
//  LGKKeyboardFit
//
//  Created by 刘亚军 on 2022/5/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (LGKKeyboard)
@property (nonnull, nonatomic, readonly, copy) NSArray<__kindof UIView*> * sortedArrayByTag;
@property (nonnull, nonatomic, readonly, copy) NSArray<__kindof UIView*> * sortedArrayByPosition;
@end

NS_ASSUME_NONNULL_END
