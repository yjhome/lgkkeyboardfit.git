//
//  LGKViewController.m
//  LGKKeyboardFit
//
//  Created by lyj on 05/24/2022.
//  Copyright (c) 2022 lyj. All rights reserved.
//

#import "LGKViewController.h"
#import <LGKKeyboardFit/LGKKeyboardFit.h>

@interface LGKViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;
@property (weak, nonatomic) IBOutlet LGKFTextView *textView;

@end

@implementation LGKViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [LGKKeyboardFit sharedFit].enable = YES;
    [LGKKeyboardFit sharedFit].enableAutoToolbar = YES;
    [LGKKeyboardFit sharedFit].toolbarManageBehaviour = LGKAutoToolbarByPosition;
    
    self.textField2.keyboardBarEnable = YES;
    self.textView.keyboardBarEnable = YES;
}

@end
