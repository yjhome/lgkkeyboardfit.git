//
//  main.m
//  LGKKeyboardFit
//
//  Created by lyj on 05/24/2022.
//  Copyright (c) 2022 lyj. All rights reserved.
//

@import UIKit;
#import "LGKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LGKAppDelegate class]));
    }
}
