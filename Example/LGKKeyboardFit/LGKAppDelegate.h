//
//  LGKAppDelegate.h
//  LGKKeyboardFit
//
//  Created by lyj on 05/24/2022.
//  Copyright (c) 2022 lyj. All rights reserved.
//

@import UIKit;

@interface LGKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
