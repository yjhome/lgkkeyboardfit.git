# LGKKeyboardFit

[![CI Status](https://img.shields.io/travis/lyj/LGKKeyboardFit.svg?style=flat)](https://travis-ci.org/lyj/LGKKeyboardFit)
[![Version](https://img.shields.io/cocoapods/v/LGKKeyboardFit.svg?style=flat)](https://cocoapods.org/pods/LGKKeyboardFit)
[![License](https://img.shields.io/cocoapods/l/LGKKeyboardFit.svg?style=flat)](https://cocoapods.org/pods/LGKKeyboardFit)
[![Platform](https://img.shields.io/cocoapods/p/LGKKeyboardFit.svg?style=flat)](https://cocoapods.org/pods/LGKKeyboardFit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

```
[LGKKeyboardFit sharedFit].enable = YES; //是否开启键盘自适应
[LGKKeyboardFit sharedFit].enableAutoToolbar = YES; // 是否开启键盘辅助工具栏
[LGKKeyboardFit sharedFit].toolbarManageBehaviour = LGKAutoToolbarByPosition; //自适应键盘文本框排列类型
    
self.textField2.keyboardBarEnable = YES; //textField开启自适应键盘
self.textView.keyboardBarEnable = YES; //textView开启自适应键盘
```

## Requirements

## Installation

LGKKeyboardFit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LGKKeyboardFit'
```

## Author

lyj, liuyajun1999@icloud.com

## License

LGKKeyboardFit is available under the MIT license. See the LICENSE file for more info.
